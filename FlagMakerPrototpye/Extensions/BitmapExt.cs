﻿using System.Drawing;
using System.Drawing.Imaging;

namespace FlagMakerPrototpye
{
    public static class BitmapExt
    {
        /// <summary>
        /// Replaces the color of the image with another
        /// </summary>
        /// <param name="bmp">The image</param>
        /// <param name="colorIn">The color to replace</param>
        /// <param name="colorOut">The color to use</param>
        public static void ReplaceColor(this Bitmap bmp, Color colorIn, Color colorOut)
        {
            var imageAttributes = new ImageAttributes();
            var colorMap = new ColorMap()
            {
                OldColor = colorIn,
                NewColor = colorOut
            };

            ColorMap[] remapTable = { colorMap };
            imageAttributes.SetRemapTable(remapTable, ColorAdjustType.Bitmap);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.DrawImage(
                    bmp,
                    new Rectangle(0, 0, bmp.Width, bmp.Height),
                    0, 0, bmp.Width, bmp.Height,
                    GraphicsUnit.Pixel,
                    imageAttributes);
            }
        }
    }
}
