﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FlagMakerPrototpye
{
    class FlagComponentsLoader
    {
        private List<FlagPattern> _flagPatterns = new List<FlagPattern>();
        private List<Symbol> _symbols = new List<Symbol>();
        private List<CultureColors> _colors = new List<CultureColors>();

        public void Run()
        {
            Parallel.Invoke(
                LoadFlagPatterns,
                LoadFlagSymbols,
                LoadColors);

            BuildFlagComponentRelations();
        }

        /// <summary>
        /// Loads all flag patterns from patterns.json
        /// </summary>
        public void LoadFlagPatterns()
        {
            using (StreamReader file = File.OpenText(@"Patterns\patterns.json"))
            {
                var serializer = new JsonSerializer();
                _flagPatterns = (List<FlagPattern>)serializer.Deserialize(file, typeof(List<FlagPattern>));
            }
        }

        /// <summary>
        /// Loads all flag symbols from symbols.json
        /// </summary>
        public void LoadFlagSymbols()
        {
            using (StreamReader file = File.OpenText(@"Symbols\symbols.json"))
            {
                var serializer = new JsonSerializer();
                _symbols = (List<Symbol>)serializer.Deserialize(file, typeof(List<Symbol>));
            }
        }

        /// <summary>
        /// Loads flag colors from Colors.json
        /// </summary>
        public void LoadColors()
        {
            using (StreamReader file = File.OpenText(@"Colors\Colors.json"))
            {
                var serializer = new JsonSerializer();
                _colors = (List<CultureColors>)serializer.Deserialize(file, typeof(List<CultureColors>));
            }
            foreach (var colorGroup in _colors.SelectMany(x => x.ColorGroups))
            {
                foreach (var colorObject in colorGroup.ColorObjects)
                {
                    var color = Color.FromArgb(colorObject.R, colorObject.G, colorObject.B);
                    colorGroup.Colors.Add(color);
                }
            }
        }

        /// <summary>
        /// Builds relations between the loaded flag components
        /// </summary>
        public void BuildFlagComponentRelations()
        {
            //Gets all unique categories
            var allCategories = _flagPatterns.SelectMany(x => x.Categories).Distinct().ToList();

            //Assigns each flag category its respective flag components
            foreach (string category in allCategories)
            {
                R.FlagComponentsList.Add(new FlagComponents()
                {
                    Category = category,
                    FlagPatterns = _flagPatterns.Where(x => x.Categories.Contains(category)).ToList(),
                    Colors = _colors.FirstOrDefault(x => x.Name.Equals(category)),
                    Symbols = _symbols.Where(x => x.Categories.Contains(category)).ToList()
                });
            }
        }
    }
}
