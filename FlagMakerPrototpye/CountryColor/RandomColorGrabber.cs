﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace FlagMakerPrototpye
{
    //coll = Collection
    //idx = Index
    //rand = Random
    class RandomColorGrabber
    {
        private static readonly Random Random = new Random();

        /// <summary>
        /// Returns an array of three random colors from a color collection
        /// </summary>
        /// <param name="cultureColors">The color culture group</param>
        /// <returns>An array of three random colors</returns>
        public static Color[] GetTrioColors(CultureColors cultureColors)
        {
            List<List<Color>> regularColorGroups = cultureColors.ColorGroups
                .Where(x => !x.IsRare)
                .Select(y => y.Colors).ToList();

            List<List<Color>> rareColorGroups = cultureColors.ColorGroups
                .Where(x => x.IsRare)
                .Select(y => y.Colors).ToList();

            var finalColorColl = new List<List<Color>>();

            int rareColorChance = Random.Next(0, 100);

            //Adds rare colors based on the rare color chance
            finalColorColl.AddRange(
                cultureColors.RareColorChance > rareColorChance
                ? regularColorGroups.Concat(rareColorGroups)
                : regularColorGroups);

            //Gets a random color index for the first color
            int randCollIdx1 = Random.Next(0, finalColorColl.Count);
            int randCollIdx2 = -1;
            int randCollIdx3 = -1;

            //Prevents the second collection from being the same as the first
            while (randCollIdx2 == randCollIdx1 ||
                   randCollIdx2 == -1)
            {
                randCollIdx2 = Random.Next(0, finalColorColl.Count);
            }

            //Prevents the third collection from being the same as the first two
            while (randCollIdx3 == randCollIdx1 ||
                   randCollIdx3 == randCollIdx2 ||
                   randCollIdx3 == -1)
            {
                randCollIdx3 = Random.Next(0, finalColorColl.Count);
            }

            List<Color> coll1 = finalColorColl[randCollIdx1];
            List<Color> coll2 = finalColorColl[randCollIdx2];
            List<Color> coll3 = finalColorColl[randCollIdx3];

            int randColorIdx1 = Random.Next(0, coll1.Count);
            int randColorIdx2 = Random.Next(0, coll2.Count);
            int randColorIdx3 = Random.Next(0, coll3.Count);

            return new Color[]
            {
                coll1[randColorIdx1],
                coll2[randColorIdx2],
                coll3[randColorIdx3]
            };
        }
    }
}
