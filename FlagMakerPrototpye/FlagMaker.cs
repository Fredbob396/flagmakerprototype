﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace FlagMakerPrototpye
{
    class FlagMaker
    {
        private Random _random = new Random();

        public void TestRun()
        {
            //In the final program, this would represent a property assigned probably to a tech group
            var components = R.FlagComponentsList.FirstOrDefault(c => c.Category.Equals("WestEurope"));

            for (int i = 0; i < 15; i++)
            {
                CreateFlag(components);
            }
        }

        /// <summary>
        /// Creates a flag out of a given FlagComponents object
        /// </summary>
        /// <param name="flagComponents">The components to make a flag out of</param>
        public void CreateFlag(FlagComponents flagComponents)
        {
            var flag = new Flag();
            AssignFlagPattern(flag, flagComponents);
            AssignFlagColors(flag, flagComponents);
            using (var orig = new Bitmap($@"Patterns\{flag.Pattern.Name}.png"))
            {
                using (var flagBmp = new Bitmap(orig.Width, orig.Height, PixelFormat.Format24bppRgb))
                {
                    DrawOriginalPattern(orig, flagBmp);
                    ParsePatternFlipping(flag, flagBmp);
                    ApplyFlagColors(flag, flagBmp);
                    AssignFlagSymbol(flag, flagComponents);
                    ApplyFlagSymbol(flag, flagBmp);
                    WriteFlagToFile(flag, flagBmp);
                }
            }
        }

        /// <summary>
        /// Draws the unedited pattern to the bitmap
        /// </summary>
        private void DrawOriginalPattern(Bitmap orig, Bitmap flagBmp)
        {
            using (Graphics gr = Graphics.FromImage(flagBmp))
            {
                gr.DrawImage(orig, new Rectangle(0, 0, flagBmp.Width, flagBmp.Height));
            }
        }

        /// <summary>
        /// Assigns a random pattern to the flag
        /// </summary>
        private void AssignFlagPattern(Flag flag, FlagComponents components)
        {
            int patternIdx = _random.Next(0, components.FlagPatterns.Count);
            flag.Pattern = components.FlagPatterns[patternIdx].CloneJson();
        }

        /// <summary>
        /// Assigns a random trio of colors to the flag
        /// </summary>
        private void AssignFlagColors(Flag flag, FlagComponents components)
        {
            flag.Colors = RandomColorGrabber.GetTrioColors(components.Colors);
        }

        /// <summary>
        /// Parses the flag flipping parameters
        /// </summary>
        private void ParsePatternFlipping(Flag flag, Bitmap flagBmp)
        {
            if (flag.Pattern.CanFlipHorizontal && _random.Next(0, 100) > 50)
            {
                flagBmp.RotateFlip(RotateFlipType.RotateNoneFlipX);
                ChangeSymbolPosition(flag, "Horizontal");
            }

            if (flag.Pattern.CanFlipVertical && _random.Next(0, 100) > 50)
            {
                flagBmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
                ChangeSymbolPosition(flag, "Vertical");
            }

            if (flag.Pattern.CanRotate90 && _random.Next(0, 100) > 50)
            {
                flagBmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                ChangeSymbolPosition(flag, "90");
            }
        }

        /// <summary>
        /// Changes the flag's symbol positions based on the rotation applied
        /// </summary>
        private void ChangeSymbolPosition(Flag flag, string type)
        {
            var newTypes = new List<string>();
            Dictionary<string, string> map;

            switch (type)
            {
                case "Horizontal":
                    map = new Dictionary<string, string>
                    {
                        { "L", "R" },{ "R", "L" },
                        { "UL", "UR" },{ "UR", "UL" },
                        { "DL", "DR" },{ "DR", "DL" }
                    };
                    break;
                case "Vertical":
                    map = new Dictionary<string, string>
                    {
                        { "U", "D" },{ "D", "U" },
                        { "UL", "DL" },{ "UR", "DR" },
                        { "DL", "UL" },{ "DR", "UR" }
                    };
                    break;
                case "90":
                    map = new Dictionary<string, string>
                    {
                        { "L", "U" },{ "R", "D" },
                        { "U", "R" },{ "D", "L" },
                        { "UL", "UR" },{ "UR", "DR" },
                        { "DL", "UL" },{ "DR", "DL" }
                    };
                    break;
                default:
                    map = new Dictionary<string, string>();
                    break;
            }

            foreach (string sType in flag.Pattern.SymbolTypes)
            {
                string outVal;
                newTypes.Add(map.TryGetValue(sType, out outVal) ? outVal : sType);
            }

            flag.Pattern.SymbolTypes.Clear();
            flag.Pattern.SymbolTypes.AddRange(newTypes);
        }

        /// <summary>
        /// Applies the flag's colors to the flag bitmap
        /// </summary>
        private void ApplyFlagColors(Flag flag, Bitmap flagBmp)
        {
            flagBmp.ReplaceColor(Color.FromArgb(255, 0, 0), flag.Colors[0]);
            flagBmp.ReplaceColor(Color.FromArgb(0, 255, 0), flag.Colors[1]);
            flagBmp.ReplaceColor(Color.FromArgb(0, 0, 255), flag.Colors[2]);
        }

        /// <summary>
        /// Assigns a flag symbol to the flag
        /// </summary>
        private void AssignFlagSymbol(Flag flag, FlagComponents components)
        {
            var symbols = (
                from s in components.Symbols
                where flag.Pattern.SymbolTypes.Contains(s.Type)
                //where flag.Pattern.Categories.Intersect(components.Symbols.SelectMany(x => x.Categories)).Any()
                select s).ToList();

            if(symbols.Count == 0) return;

            int symbolIdx = _random.Next(0, symbols.Count);
            flag.Symbol = symbols[symbolIdx];
        }

        /// <summary>
        /// Applies the flag's symbol to the flag
        /// </summary>
        private void ApplyFlagSymbol(Flag flag, Bitmap flagBmp)
        {
            if(flag.Symbol == null) return;

            using (Image symbol = Image.FromFile($@"Symbols\{flag.Symbol.Name}.png"))
            {
                using (Graphics g = Graphics.FromImage(flagBmp))
                {
                    g.DrawImage(symbol, new Rectangle(0, 0, flagBmp.Width, flagBmp.Height));
                }
            }
        }

        /// <summary>
        /// Writes the flag bitmap to a file
        /// </summary>
        private void WriteFlagToFile(Flag flag, Bitmap flagBmp)
        {
            BitmapSource flagBmpSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                flagBmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            var flagWbmp = new WriteableBitmap(flagBmpSource);

            //TODO: Better test naming
            using (var fs = new FileStream($@"GeneratedFlags\{flag.Pattern.Name}{_random.Next(0,100)}.tga", FileMode.Create))
            {
                flagWbmp.WriteTga(fs);
            }
        }
    }
}
