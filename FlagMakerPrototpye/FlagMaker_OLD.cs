﻿//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Drawing.Imaging;
//using System.IO;
//using System.Linq;
//using System.Threading.Tasks;
//using System.Windows;
//using System.Windows.Media.Imaging;
//using Newtonsoft.Json;

//namespace FlagMakerPrototpye
//{
//    class FlagMaker
//    {
//        //Will be removed when the move to the main project occurs
//        private readonly Random _random = new Random();

//        private List<FlagPattern> _flagPatterns = new List<FlagPattern>();
//        private List<Symbol> _symbols = new List<Symbol>();
//        private List<CultureColors> _colors = new List<CultureColors>();

//        /// <summary>
//        /// Test method for generating flags. Will be removed when the move to the main project occurs
//        /// </summary>
//        public void Test()
//        {
//            string cultureType = "Muslim";

//            Parallel.Invoke(
//                LoadFlagPatterns,
//                LoadFlagSymbols,
//                LoadColors);

//            List<FlagPattern> flagPatterns = _flagPatterns
//                .Where(p => p.Categories.Contains(cultureType)).ToList();

//            for (int i = 0; i < 15; i++)
//            {
//                Flag flag = NewFlagFromPattern(flagPatterns[_random.Next(0, flagPatterns.Count)]);
//                flag.Colors = RandomColorGrabber.GetTrioColors(_colors.FirstOrDefault(x => x.Name.Equals(cultureType)));
//                MakeFlag(flag, i);
//            }

//            FlagComponents components = R.FlagComponentsList.FirstOrDefault(x => x.Category.Equals("WestEurope"));
//        }

//        /// <summary>
//        /// Creates a flag from a pattern through cloning
//        /// </summary>
//        /// <param name="pattern">The pattern to create the flag out of</param>
//        /// <returns>The flag object</returns>
//        private Flag NewFlagFromPattern(FlagPattern pattern)
//        {
//            var flag = new Flag();
//            //Clones properties
//            PropertyCopier.CopyPropertyValues(pattern, flag);

//            //Clones lists
//            flag.Categories = new List<string>(pattern.Categories);
//            flag.SymbolTypes = new List<string>(pattern.SymbolTypes);

//            return flag;
//        }

//        /// <summary>
//        /// Loads all flag patterns from patterns.json
//        /// </summary>
//        public void LoadFlagPatterns()
//        {
//            using (StreamReader file = File.OpenText(@"Patterns\patterns.json"))
//            {
//                var serializer = new JsonSerializer();
//                _flagPatterns = (List<FlagPattern>)serializer.Deserialize(file, typeof(List<FlagPattern>));
//            }
//        }

//        /// <summary>
//        /// Loads all flag symbols from symbols.json
//        /// </summary>
//        public void LoadFlagSymbols()
//        {
//            using (StreamReader file = File.OpenText(@"Symbols\symbols.json"))
//            {
//                var serializer = new JsonSerializer();
//                _symbols = (List<Symbol>)serializer.Deserialize(file, typeof(List<Symbol>));
//            }
//        }

//        /// <summary>
//        /// Loads flag colors from Colors.json
//        /// </summary>
//        public void LoadColors()
//        {
//            using (StreamReader file = File.OpenText(@"Colors\Colors.json"))
//            {
//                var serializer = new JsonSerializer();
//                _colors = (List<CultureColors>)serializer.Deserialize(file, typeof(List<CultureColors>));
//            }
//            foreach (var colorGroup in _colors.SelectMany(x => x.ColorGroups))
//            {
//                foreach (var colorObject in colorGroup.ColorObjects)
//                {
//                    var color = Color.FromArgb(colorObject.R, colorObject.G, colorObject.B);
//                    colorGroup.Colors.Add(color);
//                }
//            }
//        }

//        /// <summary>
//        /// Creates a flag file
//        /// </summary>
//        /// <param name="flag">The flag to create a file of</param>
//        /// <param name="idx">TEMP</param>
//        public void MakeFlag(Flag flag, int idx)
//        {
//            //Converts the bmp to the proper pixel format
//            using (var orig = new Bitmap($@"Patterns\{flag.Name}.png"))
//            {
//                using (var flagBmp = new Bitmap(orig.Width, orig.Height, PixelFormat.Format24bppRgb))
//                {
//                    using (Graphics gr = Graphics.FromImage(flagBmp))
//                    {
//                        gr.DrawImage(orig, new Rectangle(0, 0, flagBmp.Width, flagBmp.Height));
//                    }

//                    ParseFlagFlipping(flag, flagBmp);

//                    flagBmp.ReplaceColor(Color.FromArgb(255, 0, 0), flag.Colors[0]);
//                    flagBmp.ReplaceColor(Color.FromArgb(0, 255, 0), flag.Colors[1]);
//                    flagBmp.ReplaceColor(Color.FromArgb(0, 0, 255), flag.Colors[2]);

//                    OverlaySymbol(flag, flagBmp);

//                    BitmapSource flagBmpSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
//                        flagBmp.GetHbitmap(),
//                        IntPtr.Zero,
//                        Int32Rect.Empty,
//                        BitmapSizeOptions.FromEmptyOptions());

//                    var flagWbmp = new WriteableBitmap(flagBmpSource);

//                    using (var fs = new FileStream($@"GeneratedFlags\{MakeTag(idx)}.tga", FileMode.Create))
//                    {
//                        flagWbmp.WriteTga(fs);
//                    }
//                }
//            }
//        }

//        /// <summary>
//        /// Overlays an image over the image
//        /// </summary>
//        /// <param name="bmp">The image to overlay over</param>
//        /// <param name="flag"></param>
//        public void OverlaySymbol(Flag flag, Bitmap bmp)
//        {
//            //Gets all symbols of a compatible type and category to the flag
//            //TODO: Adapt to FlagComponent
//            List<Symbol> symbols = (
//                from s in _symbols
//                where flag.SymbolTypes.Contains(s.Type)
//                where flag.Categories.Intersect(_symbols.SelectMany(x => x.Categories)).Any()
//                select s).ToList();

//            if (symbols.Count == 0) return;

//            int sIdx = _random.Next(0, symbols.Count);
//            using (Image symbol = Image.FromFile($@"Symbols\{symbols[sIdx].Name}.png"))
//            {
//                using (Graphics g = Graphics.FromImage(bmp))
//                {
//                    g.DrawImage(symbol, new Rectangle(0, 0, bmp.Width, bmp.Height));
//                }
//            }
//        }

//        /// <summary>
//        /// Parses the flag flipping parameters
//        /// </summary>
//        /// <param name="flag">The flag object</param>
//        /// <param name="flagBmp">The flag bitmap</param>
//        private void ParseFlagFlipping(Flag flag, Bitmap flagBmp)
//        {
//            if (flag.CanFlipHorizontal && _random.Next(0, 100) > 50)
//            {
//                flagBmp.RotateFlip(RotateFlipType.RotateNoneFlipX);
//                ChangeSymbolPosition(flag, "Horizontal");
//            }

//            if (flag.CanFlipVertical && _random.Next(0, 100) > 50)
//            {
//                flagBmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
//                ChangeSymbolPosition(flag, "Vertical");
//            }

//            if (flag.CanRotate90 && _random.Next(0, 100) > 50)
//            {
//                flagBmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
//                ChangeSymbolPosition(flag, "90");
//            }
//        }

//        /// <summary>
//        /// Changes the flag's symbol positions based on the rotation applied
//        /// </summary>
//        /// <param name="flag">The flag object to manipulate</param>
//        /// <param name="type">The type of rotation applied</param>
//        private void ChangeSymbolPosition(Flag flag, string type)
//        {
//            var newTypes = new List<string>();
//            Dictionary<string, string> map;

//            switch (type)
//            {
//                case "Horizontal":
//                    map = new Dictionary<string, string>
//                    {
//                        { "L", "R" },{ "R", "L" },
//                        { "UL", "UR" },{ "UR", "UL" },
//                        { "DL", "DR" },{ "DR", "DL" }
//                    };
//                    break;
//                case "Vertical":
//                    map = new Dictionary<string, string>
//                    {
//                        { "U", "D" },{ "D", "U" },
//                        { "UL", "DL" },{ "UR", "DR" },
//                        { "DL", "UL" },{ "DR", "UR" }
//                    };
//                    break;
//                case "90":
//                    map = new Dictionary<string, string>
//                    {
//                        { "L", "U" },{ "R", "D" },
//                        { "U", "R" },{ "D", "L" },
//                        { "UL", "UR" },{ "UR", "DR" },
//                        { "DL", "UL" },{ "DR", "DL" }
//                    };
//                    break;
//                default:
//                    map = new Dictionary<string, string>();
//                    break;
//            }

//            foreach (string sType in flag.SymbolTypes)
//            {
//                string outVal;
//                newTypes.Add(map.TryGetValue(sType, out outVal) ? outVal : sType);
//            }

//            flag.SymbolTypes.Clear();
//            flag.SymbolTypes.AddRange(newTypes);
//        }

//        /// <summary>
//        /// Returns a number formatted as a country tag
//        /// </summary>
//        /// <param name="number">The number to turn into a tag</param>
//        /// <returns>A country tag</returns>
//        private string MakeTag(int number)
//        {
//            char startChar = 'Z';
//            string tag = "";

//            while (number > 99)
//            {
//                startChar--;
//                number -= 100;
//            }

//            switch (number.ToString().Length)
//            {
//                case 1:
//                    tag += startChar + "0" + number;
//                    break;
//                case 2:
//                    tag += startChar.ToString() + number;
//                    break;
//                default:
//                    throw new ArgumentOutOfRangeException();
//            }
//            return tag;
//        }

//        //Deprecated? Rare chance?
//        public Color CompletelyRandomColor()
//        {
//            return Color.FromArgb(
//                _random.Next(0, 255),
//                _random.Next(0, 255),
//                _random.Next(0, 255)
//            );
//        }
//    }
//}
