﻿namespace FlagMakerPrototpye
{
    class Program
    {
        static void Main(string[] args)
        {
            var flagComponentsLoader = new FlagComponentsLoader();
            flagComponentsLoader.Run();

            var flagMaker = new FlagMaker();
            flagMaker.TestRun();
        }
    }
}
