﻿using System.Collections.Generic;

namespace FlagMakerPrototpye
{
    public class FlagPattern
    {
        //File Name
        public string Name { get; set; }
        //Flag can be flipped horizontal
        public bool CanFlipHorizontal { get; set; }
        //Flag can be flipped vertical
        public bool CanFlipVertical { get; set; }
        //Flag can be rotated 90 degrees to the right
        public bool CanRotate90 { get; set; }
        //Flag categories this pattern belongs to
        public List<string> Categories = new List<string>();
        //Compatible Symbol Types
        public List<string> SymbolTypes = new List<string>();
    }
}
