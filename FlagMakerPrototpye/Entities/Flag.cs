﻿using System.Drawing;

namespace FlagMakerPrototpye
{
    public class Flag
    {
        public FlagPattern Pattern;
        public Symbol Symbol;
        public Color[] Colors;
    }
}
