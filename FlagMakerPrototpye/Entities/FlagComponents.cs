﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlagMakerPrototpye
{
    public class FlagComponents
    {
        public string Category { get; set; }
        public List<FlagPattern> FlagPatterns = new List<FlagPattern>();
        public List<Symbol> Symbols = new List<Symbol>();
        public CultureColors Colors = new CultureColors();
    }
}
