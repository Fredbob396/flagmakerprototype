﻿using System.Collections.Generic;

namespace FlagMakerPrototpye
{
    public class Symbol
    {
        public List<string> Categories = new List<string>();
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
