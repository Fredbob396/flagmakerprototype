﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlagMakerPrototpye
{
    public class CultureColors
    {
        public string Name;
        public int RareColorChance;
        public List<ColorGroup> ColorGroups = new List<ColorGroup>();
    }
}
