﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlagMakerPrototpye
{
    public class ColorGroup
    {
        public string Name;
        public bool IsRare;
        public List<ColorObject> ColorObjects = new List<ColorObject>();
        public List<Color> Colors = new List<Color>();
    }
}
